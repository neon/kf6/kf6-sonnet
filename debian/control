Source: kf6-sonnet
Section: libs
Priority: optional
Maintainer: Jonathan Esk-Riddell <jr@jriddell.org>
Build-Depends: aspell,
               cmake,
               debhelper-compat (= 13),
               doxygen,
               graphviz,
               hspell,
               kf6-extra-cmake-modules,
               libaspell-dev,
               libhunspell-dev,
               libvoikko-dev,
               pkgconf,
               pkg-kde-tools-neon,
               qt6-5compat-dev,
               qt6-base-dev,
               qt6-declarative-dev,
               qt6-tools-dev,
               xauth,
               xvfb,
               zlib1g-dev
Standards-Version: 4.6.2
Homepage: http://projects.kde.org/sonnet
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/sonnet
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/sonnet.git

Package: kf6-sonnet
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: hspell, 
         ${misc:Depends}, 
         ${shlibs:Depends}
Replaces: libkf6sonnet-data,
          libkf6sonnetcore6,
          libkf6sonnetui6,
          qml6-module-org-kde-sonnet,
          sonnet-plugins,
Description: spell checking library for Qt (documentation)
 Sonnet is a Qt based library that offers easy access to spell
 checking using various plugin based backends.  It is part of KDE
 Frameworks 6.

Package: kf6-sonnet-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Depends: kf6-sonnet (= ${binary:Version}),
         qt6-5compat-dev,
         qt6-base-dev,
         qt6-declarative-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: libkf6sonnet-dev,
          libkf6sonnet-dev-bin,
          libkf6sonnet-doc,
Description: Spell checking library for Qt, devel files
 Sonnet is a Qt based library that offers easy access to spell
 checking using various plugin based backends.  It is part of KDE
 Frameworks 6.
 .
 This package contains development files for building software that uses
 libraries from the Sonnet framework.

Package: libkf6sonnet6-data
Architecture: all
Depends: kf6-sonnet, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6sonnet-dev
Architecture: all
Depends: kf6-sonnet-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6sonnet-dev-bin
Architecture: all
Depends: kf6-sonnet-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6sonnet-doc
Architecture: all
Depends: kf6-sonnet-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6sonnetcore6
Architecture: all
Depends: kf6-sonnet, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libkf6sonnetui6
Architecture: all
Depends: kf6-sonnet, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qml6-module-org-kde-sonnet
Architecture: all
Depends: kf6-sonnet, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: sonnet-plugins
Architecture: all
Depends: kf6-sonnet, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.
